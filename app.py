from flask import Flask, render_template, request, redirect, url_for
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__, template_folder='./')

SQLALCHEMY_DATABASE_URI = "mysql+mysqlconnector://{username}:{password}@{hostname}/{databasename}".format(
    username="panupng28",
    password="panupng6bank",
    hostname="panupng28.mysql.pythonanywhere-services.com",
    databasename="panupng28$bustudent",
)
app.config["SQLALCHEMY_DATABASE_URI"] = SQLALCHEMY_DATABASE_URI
app.config["SQLALCHEMY_POOL_RECYCLE"] = 299
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db = SQLAlchemy(app)

class Bustudent(db.Model):

    __tablename__ = "bustudent"
    id = db.Column(db.Integer, primary_key=True)
    fname = db.Column(db.String(50))
    lname = db.Column(db.String(50))
    address = db.Column(db.String(100))
    phone = db.Column(db.String(15))
    idstudent = db.Column(db.String(15))
    picture = db.Column(db.String(100))


@app.route('/')
def index():
    getdata = Bustudent.query.all()
    return render_template('first.html',data=getdata,count=1)


@app.route("/add", methods=["POST"])
def add():
    fname = request.form.get("fname")
    lname = request.form.get("lname")
    phone = request.form.get("phone")
    address = request.form.get("address")
    idstd = request.form.get("idstudent")
    newstudent = Bustudent(fname=fname,lname=lname,address=address,phone=phone,idstudent=idstd)
    db.session.add(newstudent)
    db.session.commit()
    return redirect(url_for("index"))

@app.route("/update/<int:student_id>")
def update(student_id):
    updatecommand = Bustudent.query.filter_by(id=student_id).first()
    updatecommand.fname = "kuy"
    updatecommand.lname = "mangmang"
    updatecommand.phone = "0935678787"
    db.session.commit()
    return redirect(url_for("index"))

@app.route("/delete/<int:student_id>")
def delete(student_id):
    delcommand = Bustudent.query.filter_by(id=student_id).first()
    db.session.delete(delcommand)
    db.session.commit()
    return redirect(url_for("index"))

if __name__ == '__main__':
    app.run(debug=True)
